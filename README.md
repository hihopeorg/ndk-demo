
- 前提条件：本ndk支持在Linux / Windows 环境下编译OHOS目标
- NDK 版本为3.2.11.9

# 1、环境准备
- 下载资源 https://gitee.com/link?target=https%3A%2F%2Frepo.huaweicloud.com%2Fopenharmony%2Fos%2F3.2-Release%2Fohos-sdk-windows_linux-public.tar.gz
- 将解压之后，在目录 ohos-sdk/中包含了 native-linux-x64-3.2.11.9-Release.zip

## Linux 环境(以Ubuntu 20.04.2 LTS 为例）：
1. mkdir ndk
2. unzip native-linux-x64-3.2.11.9-Release.zip -d ./ndk
	将native-linux-x64-3.2.11.9-Release.zip解压当前路径 ~/ndk/
3. cd ndk/native
4. 将cmake、ninja以及工具链添加到系统环境变量
    -    编辑 .bashrc. 使用命令 gedit ~/.bashrc 或者 vim ~/.bashrc
    -    在文件末尾加上如下的路径
    -    export PATH=$PATH:~/ndk/native/build-tools/cmake/bin:~/ndk/native/llvm/bin
    -    上述路径是示例路径，具体添加路径根据ndk所在路径自行添加。

5. 直接解压ndk_demo.tgz到 native目录下即可
6. 执行命令：
- build-tools/cmake/bin/cmake -G "Ninja" -DCMAKE_TOOLCHAIN_FILE:PATH=".\build\cmake\ohos.toolchain.cmake" . -DOHOS_ARCH="armeabi-v7a" // 这是编译32位的命令 请与OH版本保持一致
- build-tools/cmake/bin/cmake -G "Ninja" -DCMAKE_TOOLCHAIN_FILE:PATH="./build/cmake/ohos.toolchain.cmake"   // 这是编译64位的命令
7. 执行命令：./build-tools/cmake/bin/ninja
8. cd sample
-    在sample 目录下，可以看到可执行文件ndk_test

## Windows 环境(以Windows 10为例）：
1. 同linux操作步骤至解压完 native-linux-x64-3.2.11.9-Release.zip
2. 进入native
3. 将cmake、ninja以及工具链添加到系统环境变量
-     例如将F:\Hoperun\2022H1\ndk\windows_2022-06-15_08-43-10\native\build-tools\cmake\bin 和 F:\Hoperun\2022H1\ndk\windows_2022-06-15_08-43-10\native\llvm\bin
- 	添加到系统环境变量。上述路径是示例路径，具体添加路径根据ndk所在路径自行添加。
- 	系统环境变量添加方式：我的电脑-> 系统属性 -> 高级系统设置 -> 环境变量, 找到Path 变量，双击。 选择“新建"。添加环境变量
4. 将vendor/hihope/proprietories/release/demo/ndk 下的CMakeLists.txt和sample拷贝到上面的native目录下
5. 打开命令提示符（可以通过开始菜单查找命令提示符）
6. 切换到ndk 所在的盘符，如F盘
7. 使用cd 命令进入native 目录下， 如cd F:\Hoperun\2022H1\ndk\windows_2022-06-15_08-43-10\native
8. 执行命令：cmake -G "Ninja" -DCMAKE_TOOLCHAIN_FILE:PATH="./build/cmake/ohos.toolchain.cmake
9. 执行命令：ninja
10. 执行命令：./build-tools/cmake/bin/ninja
11. cd sample
-    在sample 目录下，可以看到可执行文件ndk_test
- NDK 默认编译arm64的目标文件，如果要编译arm 的目标，执行cmake 的命令换成如下的方式：
- cmake -G "Ninja" -DCMAKE_TOOLCHAIN_FILE:PATH=".\build\cmake\ohos.toolchain.cmake" . -DOHOS_ARCH="armeabi-v7a"

# 2、示例运行操作步骤：
1. 将对应的STL 拷贝到设备中
    -   这里要说明一下，NDK 中OHOS_STL 使用的是c++_shared，也就是libc++_shared.so
    -   这个也是clang 默认链接的c++库，但是在ohos的设备中，并没有该库，只有libc++.so ，尝试修改STL为libc++.so都没有生效，可能是链接选项没有配置好。
    -   所以先使用这种临时方案，将libc++_shared.so 拷贝到设备中。
    -   不同的c++库对应的位置：
    -   arm64: native/llvm/lib/aarch64-linux-ohos/c++/libc++_shared.so
    -   arm: native/llvm/lib/arm-linux-ohos/c++/libc++_shared.so

2. 拷贝方式：
    1. hdc shell 进入设备
    2. mount -o remount,rw /
    3. 退出hdc
    4. hdc file send native/llvm/lib/arm-linux-ohos/c++/libc++_shared.so /system/lib
        - 检查 /system/lib 目录下是否有 此动态库。hdc版本不同操作手法不同。
        -   或者 ` hdc file send native/llvm/lib/aarch64-linux-ohos/c++/libc++_shared.so /system/lib64`
    5.   将编译目标拷贝到设备
    - 如上面编译出来的ndk_test
    `hdc file send native/sample/ndk_test /system/bin`

# 3、编译自己模块
- 参考sample实现写自己的CMakeLists.txt
- native/docs 下面有ndk api 的介绍。
